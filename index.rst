.. |label| replace:: Einheiten-Auswahl in Buybox auf der Artikelseite
.. |snippet| replace:: FvUnitsOnDetailPage
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.1
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Mit diesen Plugin werden auf der Artikeldetailseite eine Auswahl von Einheiten angezeigt. Dadurch ist es möglich je nach Einheit die komplette mMenge in den Warenkorb zu legen.

Frontend
--------
Auf der Artikeldetailseite wird eine zusätzliche Auswahl-Box angezeigt mit den Einheiten die beim Artikel hinterlegt wurden.
.. image:: FvUnitsOnDetailPage1.png

Der Kunde kann nun die Standardeinheit (Stück) wählen und noch 2 zusätzliche wenn diese beim Artikel hinterlegt sind.
Wenn der Kunde eine Einheit ausgewählt hat und dann in den Warenkorb klickt, werden die entsprechenden Stückzahlen in den Warenkorb hinzugefügt, je nachdem welcher Faktor bei welcher Einheit hinterlegt ist.
.. image:: FvUnitsOnDetailPage2.png

Backend
-------
Artikel
_____________
.. image:: FvUnitsOnDetailPage3.png
Beim Artikel werden die Einheiten und die Inhalte hinterlegt. Es können max. 2 Einheiten gepflegt werden.


technische Beschreibung
------------------------
.. image:: FvUnitsOnDetailPage4.png

Es werden beim installieren automatisch 4 Freitext-Felder bei article_attributes angelegt.


Modifizierte Template-Dateien
-----------------------------
:/detail/buy.tpl:



